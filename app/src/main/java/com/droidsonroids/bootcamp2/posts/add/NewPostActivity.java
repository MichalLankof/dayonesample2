package com.droidsonroids.bootcamp2.posts.add;

import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.view.Menu;
import android.view.MenuItem;

import com.droidsonroids.bootcamp2.R;
import com.droidsonroids.bootcamp2.posts.PostDetailsActivity;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class NewPostActivity extends PostDetailsActivity {

	@Override public boolean onCreateOptionsMenu(final Menu menu) {
		getMenuInflater().inflate(R.menu.activity_new_post, menu);
		return true;
	}

	@Override public boolean onOptionsItemSelected(final MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.action_new) {
			new AsyncTask<Void, Void, Boolean>() {
				@Override protected Boolean doInBackground(final Void... params) {
					try {
						Request request = buildPostRequest();
						Response response = mHttpClient.newCall(request).execute();
						return response.isSuccessful();
					} catch (IOException | JSONException e) {
						return false;
					}
				}

				@Override protected void onPostExecute(final Boolean success) {
					if (success) {
						setResult(RESULT_OK);
						finish();
					} else {
						Snackbar.make(mPostDetailsLinearLayout, R.string.error_request_put, Snackbar.LENGTH_SHORT).show();
					}
				}
			}.execute();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	private Request buildPostRequest() throws JSONException {
		String url = buildPostRequestUrl();
		RequestBody requestBody = RequestBody.create(JSON, buildJsonRequestBody());
		return getRequestBuilderWithHeaders()
				.url(url)
				.post(requestBody)
				.build();
	}

	private String buildJsonRequestBody() throws JSONException {
		JSONObject requestBody = new JSONObject();
		requestBody.put("title", mPostTitleEditText.getText().toString());
		requestBody.put("body", mPostBodyEditText.getText().toString());
		return requestBody.toString();
	}

	private String buildPostRequestUrl() {
		return POSTS_URL;
	}
}
